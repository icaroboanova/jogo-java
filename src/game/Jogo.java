package game;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class Jogo extends JPanel{

	private Dinossauro dinossauro;
	private Cenario cenario;
	private Obstaculo obstaculo;
	private boolean k_cima = false;
	private boolean k_espaco = false;
	private boolean k_y = false;
	private boolean k_n = false;

	private boolean gameOver = false;
	private boolean caindo = false;
	private boolean inimigo = true;

	private Long divisor = (long) 0;
	private Long score = (long) 0;

	public Jogo() {
		addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {

			}			
			@Override
			public void keyReleased(KeyEvent e) {
				switch (e.getKeyCode()) {
				case KeyEvent.VK_UP: k_cima=false; break;
				case KeyEvent.VK_SPACE: k_espaco = false; break;
				case KeyEvent.VK_Y: k_y = false; break;
				case KeyEvent.VK_N: k_n = false; break;
				}
			}
			@Override
			public void keyPressed(KeyEvent e) {
				switch (e.getKeyCode()) {
				case KeyEvent.VK_UP: k_cima=true; break;
				case KeyEvent.VK_SPACE: k_espaco = true; break;
				case KeyEvent.VK_Y: k_y = true; break;
				case KeyEvent.VK_N: k_n = true; break;
				}
			}
		});
		dinossauro = new Dinossauro();
		cenario = new Cenario();
		obstaculo = new Obstaculo();
		setFocusable(true);
		setLayout(null);

		new Thread(new Runnable() { // instancia da Thread + classe interna an�nima
			@Override
			public void run() {
				gameloop(); // inicia o gameloop
			}
		}).start(); // dispara a Thread
	}

	public void gameloop() {
		while(true) { // repeti��o intermitente do gameloop
			handlerEvents();
			update();
			render();
			try {
				Thread.sleep(17);
			}catch (Exception e) {}
		}
	}

	public void handlerEvents() {
		if(!gameOver) {
			saltar();
		} else {
			escolhaUsuario();
		}
	}

	public void update() {
		if(!gameOver) {
			dinossauro.moverDino();
			cenario.efeitoDeslocamento();
			verificarColisao();
			moverObstaculo();
			aumentarPlacar();
		}
	}

	public void render() {
		repaint();
	}

	// METODO SOBRESCRITO ---------------------
	@Override
	protected void paintComponent(Graphics g) {
		if(!gameOver) {
			super.paintComponent(g);
			setBackground(Color.LIGHT_GRAY);
			g.setColor(Color.BLACK);
			g.setFont(new Font("Arial", Font.BOLD, 20));
			carregarCenario(g);
			gerarObstaculo(g);
			g.drawImage(dinossauro.img, dinossauro.posicaoX, dinossauro.posicaoY, null);
			g.drawString("Score: " + score.toString(), 270, 20);
		} else {
			super.paintComponent(g);
			setBackground(Color.BLACK);
			g.setColor(Color.RED);
			g.setFont(new Font("Arial", Font.BOLD, 40));
			g.drawString("GAME OVER", 200, 200);
			g.setColor(Color.WHITE);
			g.drawString("RETRY? (Y/N)", 200, 250);
		}
	}


	private void carregarCenario(Graphics g) {
		//carregar plano de fundo
		g.drawImage(cenario.wallpaper, 0, 0, null);

		//carregar chao
		g.drawImage(cenario.esquerda, 0, 400, null);
		g.drawImage(cenario.meio, 80, 400, null);
		g.drawImage(cenario.meio, 160, 400, null);
		g.drawImage(cenario.meio, 240, 400, null);
		g.drawImage(cenario.meio, 320, 400, null);
		g.drawImage(cenario.meio, 400, 400, null);
		g.drawImage(cenario.meio, 480, 400, null);
		g.drawImage(cenario.direita, 560, 400, null);
	}

	private void gerarObstaculo(Graphics g) {
		if(inimigo) {
			g.drawImage(obstaculo.img, obstaculo.posicaoX, obstaculo.posicaoY, null);
		} else {
			if(score%12 == 0) inimigo = true;
		}
	}

	private void passouObstaculo() {
		inimigo = false;
		obstaculo.mudarObstaculo();
		obstaculo.posicaoX = Principal.LARGURA_TELA;
	}

	private void saltar() {
		if(k_cima || k_espaco) {
			dinossauro.ativarSalto();  
			if(dinossauro.posicaoY > 150 && !caindo) {
				dinossauro.posicaoY = dinossauro.posicaoY - 7;
			} else {
				caindo = true;
				dinossauro.posicaoY = dinossauro.posicaoY + 7;
			}

			if(dinossauro.posicaoY >= 300) caindo = false;
		} else {
			if(dinossauro.posicaoY < 300) {
				dinossauro.posicaoY = dinossauro.posicaoY + 7;
			}

			if(dinossauro.posicaoY >= 300) caindo = false;
			dinossauro.continuarCorrendo();
		}
	}

	private void moverObstaculo() {
		if(inimigo) obstaculo.posicaoX = obstaculo.posicaoX - 8;
		if(obstaculo.posicaoX + obstaculo.largura <= 0) passouObstaculo();
	}

	private void aumentarPlacar() {
		divisor++;
		if(divisor%3 ==0)score++;
	}

	private void verificarColisao() {
		if(obstaculo.posicaoX > dinossauro.posicaoX - obstaculo.largura && obstaculo.posicaoX + obstaculo.largura < dinossauro.posicaoX + 80 ) {
			if(dinossauro.posicaoY + 90 >= obstaculo.posicaoY) {
				//TODO BATEU
				gameOver = true;
			}
		}
		if(obstaculo.posicaoX > dinossauro.posicaoX && obstaculo.posicaoX <= dinossauro.posicaoX + 80) {
			if(dinossauro.posicaoY + 90 >= obstaculo.posicaoY) {
				//TODO BATEU
				gameOver = true;
			}
		}
	}
	
	private void escolhaUsuario() {
		if(k_y) {
			divisor = (long) 0;
			score = (long) 0;
			passouObstaculo();
			gameOver = false;
		} else if(k_n) {
			Principal.fecharJogo();
		}
	}
}
