package game;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Obstaculo {
	
	public BufferedImage img;
	public int largura = 77;
	public int altura = 77;
	
	public int posicaoX = Principal.LARGURA_TELA;
	public int posicaoY = 323;
	
	public int tipoObstaculo = 0;
	
	public Obstaculo() {
		try {
			img = ImageIO.read(getClass().getResource("/imgs/cenario/caixote.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void mudarObstaculo() {
		if(tipoObstaculo < 2) {
			tipoObstaculo++;
		} else {
			tipoObstaculo = 0;
		}
		
		switch(tipoObstaculo) {
		case 0:
			try {
				img = ImageIO.read(getClass().getResource("/imgs/cenario/caixote.png"));
				largura = 77;
				altura = 77;
				posicaoY = 323;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 1:
			try {
				img = ImageIO.read(getClass().getResource("/imgs/cenario/pedra.png"));
				largura = 70;
				altura = 54;
				posicaoY = 346;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
}
