package game;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Dinossauro {

	public BufferedImage img;
	public int posicaoX = 20;
	public int posicaoY = 300;

	private int step = 0;
	private int velocidadeDino = 0;

	public boolean correndo = true;
	public boolean saltando = false;

	public Dinossauro() {
		try {
			img = ImageIO.read(getClass().getResource("/imgs/dinossauro/parado.png"));
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void correr() {
		if(correndo) {
			//executa a cada 5 vezes
			if(velocidadeDino < 5) {
				velocidadeDino++;
			}else {
				velocidadeDino = 0;
			}

			//troca de imagem pra dar sensa��o de velocidade
			if(velocidadeDino == 3) {
				if(step < 3) {
					step++;
				} else {
					step = 0;
				}
				try {
					img = ImageIO.read(getClass().getResource("/imgs/dinossauro/correndo" + step + ".png"));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public void saltar() {
		if(saltando) {
			try {
				img = ImageIO.read(getClass().getResource("/imgs/dinossauro/pulando.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void moverDino() {
		if(correndo) {
			correr();
		} else {
			saltar();
		}
	}

	public void ativarSalto() {	
		saltando = true;
		correndo = false;
	}

	public void continuarCorrendo() {
		saltando = false;
		correndo = true;
	}

}
