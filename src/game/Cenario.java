package game;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Cenario {
	
	public BufferedImage esquerda;
	public BufferedImage meio;
	public BufferedImage direita;
	public BufferedImage wallpaper;
	
	private int corteFundo = 0;
	private int velocidadeFundo = 0;
	
	public Cenario() {
		try {
			esquerda = ImageIO.read(getClass().getResource("/imgs/cenario/chao1.png"));
			meio = ImageIO.read(getClass().getResource("/imgs/cenario/chao2.png"));
			direita = ImageIO.read(getClass().getResource("/imgs/cenario/chao3.png"));
			wallpaper = ImageIO.read(getClass().getResource("/imgs/fundo/background0.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void efeitoDeslocamento() {
		//executa 1 a cada 5 vezes pra n�o ficar mt rapido
		if(velocidadeFundo < 5) {
			velocidadeFundo++;
		}else {
			velocidadeFundo = 0;
		}

		//troca de imagem pra dar a sensa��o de velocidade
		if(velocidadeFundo == 2) {
			if(corteFundo < 4) {
				corteFundo++;
			} else {
				corteFundo = 0;
			}
			try {
				wallpaper = ImageIO.read(getClass().getResource("/imgs/fundo/background" + corteFundo + ".png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
